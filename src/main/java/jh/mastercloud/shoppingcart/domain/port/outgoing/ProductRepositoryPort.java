package jh.mastercloud.shoppingcart.domain.port.outgoing;

import java.util.List;
import java.util.Optional;
import jh.mastercloud.shoppingcart.domain.dtos.ProductDto;

public interface ProductRepositoryPort {

	ProductDto save(ProductDto productDto);
	List<ProductDto> findAll();
	Optional<ProductDto> findById(Long id);
	void delete(Long id);

}
