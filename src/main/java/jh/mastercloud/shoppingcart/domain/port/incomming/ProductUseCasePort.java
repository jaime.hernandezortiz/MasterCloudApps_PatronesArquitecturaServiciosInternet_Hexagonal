package jh.mastercloud.shoppingcart.domain.port.incomming;

import java.util.List;
import java.util.Optional;
import jh.mastercloud.shoppingcart.domain.dtos.ProductCreationDto;
import jh.mastercloud.shoppingcart.domain.dtos.ProductDto;

public interface ProductUseCasePort {
	List<ProductDto> findAll();
	Optional<ProductDto> findById(Long id);
	void deleteById(Long id);
	ProductDto save(ProductCreationDto productDto);
}
