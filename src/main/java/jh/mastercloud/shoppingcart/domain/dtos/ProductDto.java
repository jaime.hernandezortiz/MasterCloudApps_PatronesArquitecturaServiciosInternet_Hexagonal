package jh.mastercloud.shoppingcart.domain.dtos;

public class ProductDto {

	private Long id;
	private String name;
	private String description;
	private Float price;

	public ProductDto(){

	}

	public ProductDto(String name, String description, Float price){
		this.name = name;
		this.description = description;
		this.price = price;
	}
	public ProductDto(Long id, String name, String description, Float price) {
		this(name, description, price);
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "ProductFullDto{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", price=" + price +
				'}';
	}
}
