package jh.mastercloud.shoppingcart.domain.dtos;

public class ProductCreationDto {

	private String name;
	private String description;
	private Float price;

	public ProductCreationDto(String name, String description, Float quantity) {
		this.name = name;
		this.description = description;
		this.price = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "ProductDto{" +
				"name='" + name + '\'' +
				", description='" + description + '\'' +
				", quantity=" + price +
				'}';
	}
}
