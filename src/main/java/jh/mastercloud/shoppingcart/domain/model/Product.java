package jh.mastercloud.shoppingcart.domain.model;

public class Product {

	private String name;
	private String description;
	private Float price;


	public Product(String name, String description, Float quantity) {
		this.name = name;
		this.description = description;
		this.price = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getprice() {
		return price;
	}

	public void setprice(Float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product{" +
				"name='" + name + '\'' +
				", description='" + description + '\'' +
				", price=" + price +
				'}';
	}
}
