package jh.mastercloud.shoppingcart.domain.adaptor;

import java.util.List;
import java.util.Optional;
import jh.mastercloud.shoppingcart.domain.dtos.ProductCreationDto;
import jh.mastercloud.shoppingcart.domain.dtos.ProductDto;
import jh.mastercloud.shoppingcart.domain.port.incomming.ProductUseCasePort;
import jh.mastercloud.shoppingcart.infrasctructure.adapter.service.ProductServiceAdapter;
import jh.mastercloud.shoppingcart.domain.port.outgoing.ProductRepositoryPort;

public class ProductUseCasePortImpl implements ProductUseCasePort {

	private final ProductRepositoryPort productRepositoryPort;
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ProductServiceAdapter.class);

	public ProductUseCasePortImpl(ProductRepositoryPort productRepositoryPort) {
		this.productRepositoryPort = productRepositoryPort;
	}

	@Override
	public List<ProductDto> findAll() {
		logger.info("Getting all products");
		return this.productRepositoryPort.findAll();
	}

	@Override
	public Optional<ProductDto> findById(Long id) {
		logger.info("Getting product with id: " + id);
		return this.productRepositoryPort.findById(id);
	}

	@Override
	public void deleteById(Long id) {
		logger.info("Deleting product with id: " + id);
		this.productRepositoryPort.delete(id);
	}

	@Override
	public ProductDto save(ProductCreationDto productCreationDto) {
		logger.info("Saving product: " + productCreationDto.toString());
		ProductDto productDto = new ProductDto(productCreationDto.getName(),
				productCreationDto.getDescription(),
				productCreationDto.getPrice());
		return this.productRepositoryPort.save(productDto);
	}
}
