package jh.mastercloud.shoppingcart.application.rest;

import java.util.List;
import jh.mastercloud.shoppingcart.application.dto.request.CreateProductRequestDto;
import jh.mastercloud.shoppingcart.application.dto.response.ProductResponseDto;
import jh.mastercloud.shoppingcart.infrasctructure.adapter.service.ProductServiceAdapter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/products")
public class ProductController {

	private final ProductServiceAdapter productServiceAdapter;

	public ProductController(ProductServiceAdapter productServiceAdapter) {
		this.productServiceAdapter = productServiceAdapter;
	}

	@GetMapping("/")
	public List<ProductResponseDto> findAll(){
		return this.productServiceAdapter.findAll();
	}

	@GetMapping("/{id}")
	public ProductResponseDto getProduct(@PathVariable Long id){
		return this.productServiceAdapter.findById(id);
	}

	@PostMapping("/")
	@ResponseStatus(value = HttpStatus.CREATED)
	public ProductResponseDto addProduct(@RequestBody CreateProductRequestDto createProductRequestDto){
		return this.productServiceAdapter.save(createProductRequestDto);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteProduct(@PathVariable Long id){
		this.productServiceAdapter.deleteById(id);
	}
}
