package jh.mastercloud.shoppingcart.application.dto.response;

public class ProductResponseDto {

	private Long id;
	private String name;
	private String description;
	private Float price;

	public ProductResponseDto(){

	}

	public ProductResponseDto(Long id, String name, String description, Float price) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getprice() {
		return price;
	}

	public void setprice(Float price) {
		this.price = price;
	}
}
