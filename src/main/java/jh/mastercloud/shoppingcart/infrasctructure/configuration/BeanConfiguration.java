package jh.mastercloud.shoppingcart.infrasctructure.configuration;

import jh.mastercloud.shoppingcart.domain.adaptor.ProductUseCasePortImpl;
import jh.mastercloud.shoppingcart.domain.port.incomming.ProductUseCasePort;
import jh.mastercloud.shoppingcart.domain.port.outgoing.ProductRepositoryPort;
import jh.mastercloud.shoppingcart.infrasctructure.adapter.h2.ProductRepository;
import jh.mastercloud.shoppingcart.infrasctructure.adapter.h2.ProductRepositoryH2Adapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

	@Bean
	public ProductRepositoryPort productDatabasePort(ProductRepository productRepository){
		return new ProductRepositoryH2Adapter(productRepository);
	}

	@Bean
	public ProductUseCasePort productUseCasePort(ProductRepositoryPort productRepositoryPort){
		return new ProductUseCasePortImpl(productRepositoryPort);
	}

}
