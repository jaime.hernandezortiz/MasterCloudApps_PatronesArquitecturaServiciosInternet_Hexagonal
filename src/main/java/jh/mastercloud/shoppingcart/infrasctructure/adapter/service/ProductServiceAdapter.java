package jh.mastercloud.shoppingcart.infrasctructure.adapter.service;

import java.util.List;
import java.util.stream.Collectors;
import jh.mastercloud.shoppingcart.application.dto.request.CreateProductRequestDto;
import jh.mastercloud.shoppingcart.application.dto.response.ProductResponseDto;
import jh.mastercloud.shoppingcart.domain.dtos.ProductCreationDto;
import jh.mastercloud.shoppingcart.domain.dtos.ProductDto;
import jh.mastercloud.shoppingcart.domain.port.incomming.ProductUseCasePort;
import jh.mastercloud.shoppingcart.infrasctructure.adapter.exception.ProductNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceAdapter {

	private final ProductUseCasePort productUseCase;

	public ProductServiceAdapter(ProductUseCasePort productUseCase) {
		this.productUseCase = productUseCase;
	}

	public List<ProductResponseDto> findAll() {
		return this.productUseCase.findAll()
				.stream()
				.map(this::toProductResponseDto)
				.collect(Collectors.toList());
	}

	public ProductResponseDto findById(Long id) {
		ProductDto productDto = this.productUseCase.findById(id).orElseThrow(ProductNotFoundException::new);
		return this.toProductResponseDto(productDto);
	}

	public void deleteById(Long id) {
		this.productUseCase.deleteById(id);
	}

	public ProductResponseDto save(CreateProductRequestDto createProductRequestDto) {
		ProductCreationDto productCreationDto = new ProductCreationDto(createProductRequestDto.getName(),
				createProductRequestDto.getDescription(), createProductRequestDto.getPrice());

		ProductDto productDto = this.productUseCase.save(productCreationDto);
		return this.toProductResponseDto(productDto);
	}

	private ProductResponseDto toProductResponseDto(ProductDto productDto){
		return new ProductResponseDto(productDto.getId(), productDto.getName(), productDto.getDescription(), productDto.getPrice());
	}

}
