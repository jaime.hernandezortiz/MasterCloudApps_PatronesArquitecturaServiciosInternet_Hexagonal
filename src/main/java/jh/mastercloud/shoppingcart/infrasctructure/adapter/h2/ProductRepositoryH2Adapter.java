package jh.mastercloud.shoppingcart.infrasctructure.adapter.h2;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import jh.mastercloud.shoppingcart.domain.dtos.ProductDto;
import jh.mastercloud.shoppingcart.domain.port.outgoing.ProductRepositoryPort;

public class ProductRepositoryH2Adapter implements ProductRepositoryPort {

  private final ProductRepository productRepository;

	public ProductRepositoryH2Adapter(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public ProductDto save(ProductDto productDto) {
		ProductEntity entity = this.productRepository.save(this.toEntity(productDto));
		return this.toDto(entity);
	}

	@Override
	public List<ProductDto> findAll() {
		return this.productRepository.findAll()
				.stream()
				.map(this::toDto)
				.collect(Collectors.toList());
	}

	@Override
	public Optional<ProductDto> findById(Long id) {
		return this.productRepository.findById(id).map(this::toDto);
	}

	@Override
	public void delete(Long id) {
		this.productRepository.deleteById(id);
	}

	private ProductEntity toEntity(ProductDto productDto){
		return new ProductEntity(productDto.getId(), productDto.getName(), productDto.getDescription(), productDto.getPrice());
	}

	private ProductDto toDto(ProductEntity productEntity){
		return new ProductDto(productEntity.getId(), productEntity.getName(), productEntity.getDescription(), productEntity.getPrice());
	}
}